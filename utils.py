# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 10:41:36 2019

@author: lishijie
@date: 2019/10/16
@description: 图像数据预处理
"""
import os

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

FILE_NAME = "./data/Definition Checking/captia/0.9/1570790918029893.jpg"

def preprocess_for_train(image, height, width, bbox):
    # 如果没有提供标注框，则认为整个图像为需要关注的部分
    if bbox is None:
        bbox = tf.constant([0.0, 0.0, 1.0, 1.0],
                           dtype=tf.float32, shape=[1, 1, 4])
        
    # 转换图像张量的类型
    if image.dtype != tf.float32:
        image = tf.image.convert_image_dtype(image, dtype=tf.float32)
        
    # 使用随机的图像大小调整算法来将图像调整为VGG需要的格式
    resized_image = tf.image.resize_images(
            image, [height, width], method=np.random.randint(4))
    
    # 随机左右翻转图像
#    resized_image = tf.image.random_flip_left_right(resized_image)
    
    # 对图像分别进行上下左右翻转
    left_image = tf.image.flip_left_right(resized_image)
    up_image = tf.image.flip_up_down(resized_image)
    trans_image = tf.image.transpose_image(resized_image)
    
    return [resized_image, left_image, up_image, trans_image]

#image_raw_data = tf.gfile.FastGFile(FILE_NAME, 'rb').read()
#with tf.Session() as sess:
#    image_data = tf.image.decode_jpeg(image_raw_data)
#    
#    # 将图像的尺寸调整为224×224
#    image, left_image, up_image, trans_image = preprocess_for_train(image_data, 224, 224, None)
#    plt.imshow(trans_image.eval())
#    plt.show()
