# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 09:21:59 2019

@author: lishijie
@date: 2019/10/16
@description: 用于训练VGG16 2分类模型
              0: 模糊  1: 清晰
"""
import sys
sys.path.append('../')

import tensorflow as tf
import numpy as np
import os
from datetime import datetime

import config
from model.VGG16 import VGG16

file_path = "../data/Definition Checking/trainData/"  # 定义训练数据的存储路径

# 配置超参数
config_def = config.definition()
batch_size = config_def.batch_size
lr = config_def.lr
n_cls = config_def.n_cls
max_steps = config_def.max_steps
epoch = config_def.epoch

# 配置tensorflow的运行参数
config_tf = tf.ConfigProto()
config_tf.gpu_options.allow_growth = True

# 获取训练数据
def read_and_decode(filename):
    #根据文件名生成一个队列
    filename_queue = tf.train.string_input_producer(filename)

    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(
        serialized_example,
        features={
            'label': tf.FixedLenFeature([], tf.int64),
            'image_raw': tf.FixedLenFeature([], tf.string),
            'height': tf.FixedLenFeature([], tf.int64),
            'width': tf.FixedLenFeature([], tf.int64),
            'channels': tf.FixedLenFeature([], tf.int64)
        })

    label, image_raw = features['label'], features['image_raw']
#    height, width, channels = features['height'], features['width'], features['channels']

    image = tf.decode_raw(image_raw, tf.uint8)
    image = tf.reshape(image, [224, 224, 3])
    # 转化为float32类型，并做归一化处理
    image = tf.cast(image, tf.float32) * (1. / 255)
    label = tf.cast(label, tf.int64)
    return image, label

# 构建模型的训练过程
def train():
    x = tf.placeholder(dtype=tf.float32, shape=[None, 224, 224, 3], name='input')
    y = tf.placeholder(dtype=tf.float32, shape=[None, n_cls], name='label')
    keep_prob = tf.placeholder(tf.float32)
    output = VGG16(x, keep_prob, n_cls)

    # 损失函数
    # loss = tf.reduce_mean(tf.square(output - y))
    # train_step = tf.train.GradientDescentOptimizer(learning_rate=lr).minimize(loss)
    # 使用交叉熵损失
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=output, labels=y))
    train_step = tf.train.GradientDescentOptimizer(learning_rate=lr).minimize(loss)

    # mse = tf.sqrt(tf.reduce_mean(tf.square(output - y)))
    # 计算分类的正确率
    accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(output, 1), tf.argmax(y, 1)), tf.float32))

    # 获取训练数据
    files = os.listdir(file_path)
    data_files = [file_path+x for x in files]
    images, labels = read_and_decode(data_files)
    capacity = 1000 + 3 * batch_size
    img_batch, label_batch = tf.train.shuffle_batch([images, labels],
                                                    batch_size=batch_size,
                                                    capacity=300,
                                                    min_after_dequeue=100)
    # 进行one-hot处理
    label_batch = tf.one_hot(label_batch, n_cls, 1, 0)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    with tf.Session(config=config_tf) as sess:
        sess.run(init)
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)
        for i in range(max_steps * epoch):
            batch_x, batch_y = sess.run([img_batch, label_batch])
            batch_y = np.reshape(batch_y, (-1, 1))
            _, loss_val = sess.run([train_step, loss], feed_dict={x:batch_x, y:batch_y, keep_prob:0.8})
            if i%10 == 0:
                train_arr = accuracy.eval(feed_dict={x:batch_x, y: batch_y, keep_prob: 1.0})
                # %g表示使用指数或浮点数进行输出
                print("%s: Step [%d]  Loss : %f, training accuracy : %g" % (datetime.now(), i, loss_val, train_arr))
            if (i + 1) % max_steps == 0:
                saver.save(sess, '../data/trainedModel/definition/vgg16-', global_step=i)
        coord.request_stop()
        coord.join(threads)

if __name__ == '__main__':
    train()
