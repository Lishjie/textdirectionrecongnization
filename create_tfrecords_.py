# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 15:24:59 2019

@author: lishijie
@date: 2019/10/18
@description: 用于生成tfrecord格式的数据
"""
import sys
sys.path.append('../')

import os
import glob
import tensorflow as tf
from PIL import Image

TRAIN_PATH = "./data/Definition Checking/trainData/"
TEST_PATH = "./data/Definition Checking/testData/"

# 创建TFRecord文件的帮助函数
def _float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def _int64_features(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

"""
利用PIL实现图片的随机裁剪
参数列表：image 原始的图像
         padding 为图片添加外边距，建议padding的值为0
"""
def random_crop(image, padding=None):
    oshape = image.image_size

    # 将图片较短的边作为裁剪的图片大小
    crop_shape = oshape[0] if oshape[0] < oshape[1] else oshape[1]

    oshape_pad = (oshape[0] + 2 * padding, oshape[1] + 2 * padding)
    img_pad = Image.new("RGB", (oshape_pad[0], oshape_pad[1]))
    img_pad.paste(image, (padding, padding))

    nh = random.randint(0, oshape_pad[0] - crop_shape)
    nw = nh
    image_crop = img_pad.crop((nh, nw, nh+crop_shape[0], nw+crop_shape[1]))

    return image_crop

# 创建TFRecord格式的文件
def create_tf(imgpath):
    max_num = 1000              # 每个文件的最大图片个数
    image_num = 0               # 当前文件已存储的图片个数
    file_num = 0                # 当前已经生成的文件个数
    image_size = 224            # 神经网络的图像尺寸
    channels = 3                # 神经网络的图像深度
    rotate = [0, 90, 180, 270]  # 设定图像的旋转角度
    # 每1000张图片存储为一个文件
    train_file_name = ("train.tfrecords-%.3d" % file_num)
    # 测试数据较少，所以存储成一个文件
    test_file_name = ("test.tfrecords")

    # 获取文件夹中包含的所有目录和文件（包括当前文件夹本身）
    sub_dirs = [x[0] for x in os.walk(imgpath)]
    sub_dirs = [x.replace('\\', '/') for x in sub_dirs]
    is_root_dir = True  # 用于跳过本身的文件夹

    train_writer = tf.python_io.TFRecordWriter(TRAIN_PATH+train_file_name)
    test_writer = tf.python_io.TFRecordWriter(TEST_PATH+test_file_name)
    # 读取所有的子目录
    for sub_dir in sub_dirs:
        if is_root_dir:
            is_root_dir = False
            continue

        # 获取一个子目录中的所有图片文件
        extentions = ['jpg', 'jpeg', 'JPG', 'JPEG']
        file_list = []
        dir_name = os.path.basename(sub_dir)
        for extention in extentions:
            file_glob = os.path.join(sub_dir, '*.' + extention)
            file_list.extend(glob.glob(file_glob))
        # 如果当前文件夹内没有图片文件，则开始遍历下一个文件夹
        if not file_list: continue

        # 处理数据并进行存储
        is_first = True
        for file_name in file_list:
            img = Image.open(file_name)                 # 获取图像数据
            # 根据label的类型来判断是否进行图像的裁剪
            img_list = []
            if not is_first:
                if int(dir_name) == 0:
                    img = img.resize((image_size, image_size), Image.ANTIALIAS)  # 设定图像尺寸
                    img_list.append(img)
                else:
                    # 如果为清晰的图片，则进行图像的随机截取
                    for i in range(3):
                        img_crop = random_crop(img, padding=0)
                        img_crop = img_crop.resize((image_size, image_size), Image.ANTIALIAS)
                        img_list.append(img_crop)

            # 取每个文件夹中的第一个图片作为测试数据
            if is_first:
                is_first = False
                image_raw = img.tobytes()
                example = tf.train.Example(features=tf.train.Features(feature={
                        'label': _int64_features(int(dir_name)),
                        'image_raw': _bytes_feature(image_raw),
                        'height': _int64_features(image_size),
                        'width': _int64_features(image_size),
                        'channels': _int64_features(channels) }))
                test_writer.write(example.SerializeToString())
            # 依次旋转图片进行数据增广
            else:
                for img_ in img_list:
                    for i in range(len(rotate)):
                        # 判断文件的存储名称
                        image_num = image_num + 1
                        if image_num > max_num:
                            image_num = 1
                            file_num = file_num + 1
                            train_file_name = ("train.tfrecords-%.3d" % file_num)
                            train_writer = tf.python_io.TFRecordWriter(TRAIN_PATH+train_file_name)
                        img_rotate = img_.rotate(rotate[i])
                        image_raw = img_rotate.tobytes()
                        example = tf.train.Example(features=tf.train.Features(feature={
                            'label': _int64_features(int(dir_name)),
                            'image_raw': _bytes_feature(image_raw),
                            'height': _int64_features(image_size),
                            'width': _int64_features(image_size),
                            'channels': _int64_features(channels) }))
                        train_writer.write(example.SerializeToString())
    test_writer.close()
    train_writer.close()

if __name__ == '__main__':
    imagepath = "./data/Definition Checking/"
    create_tf(imagepath)
#    for serialized_example in tf.python_io.tf_record_iterator("./data/Definition Checking/trainData/train.tfrecords-000"):
#        example = tf.train.Example()
#        example.ParseFromString(serialized_example)
#
#        width = example.features.feature['width'].int64_list.value
#        print(width)
