# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 17:18:58 2019

@author: lishijie
@date: 2019/10/17
@description: 测试文件
"""
import sys
sys.path.append('../')
sys.path.append('./')

import os
import cv2
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

import config
from model.VGG16 import VGG16

# 配置超参数
config_def = config.definition()
batch_size = config_def.batch_size
lr = config_def.lr
n_cls = config_def.n_cls
max_steps = config_def.max_steps

# 配置tensorflow的运行参数
config_tf = tf.ConfigProto()
config_tf.gpu_options.allow_growth = True

# 测试图片的存放位置
TRST_FILE = "../data/testData/definition/"
# 训练好的模型存放的位置
TRAINED_MODEL = "../data/trainedModel/definition/v1-30000/vgg16--25999"

# 测试程序
def test():

    x = tf.placeholder(dtype=tf.float32, shape=[None, 224, 224, 3], name='input')
    keep_prob = tf.placeholder(tf.float32)
    output = VGG16(x, keep_prob, n_cls)
    # 取输出值的最大值作为分类结果
    score = tf.nn.softmax(output)
    f_cls = tf.argmax(score, 1)

    # 获取已经训练好的文件
    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    # 加载训练好的模型
    saver.restore(sess, TRAINED_MODEL)
    # 加载测试数据并进行测试
    for i in os.listdir(TRST_FILE):
        imgpath = os.path.join(TRST_FILE, i)
        im = cv2.imread(imgpath)
#        plt.imshow(im)
#        plt.show()
        im = cv2.resize(im, (224, 224)) * (1. / 255)

        im = np.expand_dims(im, axis=0)
        # 测试时keep_prob设置为1.0
        # result = sess.run([output], feed_dict={x:im, keep_prob:1.0})
        pred, _score = sess.run([f_cls, score], feed_dict={x:im, keep_prob:1.0})
        prob = round(np.max(_score), 4)
        # print("图像%s的清晰度预测为: %.3f" % (i, result[0][0][0]))
        print("图像%s的置信度判别结果为: %d, 概率为: %.4f" % (i, pred, prob))
    sess.close()

if __name__ == '__main__':
    test()
