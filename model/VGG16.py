# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 08:11:09 2019

@author: lishijie
@date: 2019/10/16
@description: VGG16 二分类图片置信度模型
              0: 图片模糊 1: 图片清晰
"""
import tensorflow as tf

# 打印每层信息
def print_layer(t):
    print(t.op.name, ' ', t.get_shape().as_list(), '\n')

# 定义卷积层
"""
可设置权重初始化的3种方式:
    1.预训练模型参数
    2.截尾正态
    3.Xavier
参数列表：
    x: 上一层的输入数据
    d_out: 本层的输出数据
    name: 卷积层的名称
    fine_tune: 使用模型参数
    xavier: 使用xavier初始化模型参数
"""
def conv(x, d_out, name, fineturn=False, xavier=False):
    d_in = x.get_shape()[-1].value
    """
    name_scope: 仅可以使Variable具有重复的命名(通过域来区分)
    variable_scope: 可以使Variable和get_variable具有重复的命名(通过域来区分)
    """
#    with tf.name_scope(name) as scope:
#        # Fine-tuning
##        if fineturn:
##            kernel = tf.constant(data_dict[name][0], name="weights")
##            bias = tf.constant(data_dict[name][1], name="bias")
##            print("fineturn")
#        if not xavier:
#            # 定义卷积核
#            kernel = tf.Variable(tf.truncated_normal([3, 3, d_in, d_out], stddev=0.1), name='weights')
#            # 定义偏置
#            bias = tf.Variable(tf.constant(0.0, dtype=tf.float32, shape=[d_out]),
#                                                trainable=True,
#                                                name='bias')
#            print("truncated_normal")
#        else:
#            kernel = tf.get_variable(scope+'weights', shape=[3, 3, d_in, d_out],
#                                                dtype=tf.float32,
#                                                initializer=tf.contrib.layers.xavier_initializer_conv2d())
#            bias = tf.Variable(tf.constant(0.0, dtype=tf.float32, shape=[d_out]),
#                                                trainable=True,
#                                                name='bias')
#            print("xavier")
#        # VGG16中的所有卷积层均使用全0填充
#        conv = tf.nn.conv2d(x, kernel, strides=[1, 1, 1, 1], padding='SAME')
#        activation = tf.nn.relu(conv + bias, name=scope)
#        print_layer(activation)
#        return activation
    with tf.variable_scope(name):
        # 定义卷积核
        kernel = tf.get_variable(
                "weight", [3, 3, d_in, d_out],
                initializer=tf.truncated_normal_initializer(stddev=0.1))
        # 定义偏置
        bias = tf.get_variable(
                "bias", [d_out], initializer=tf.constant_initializer(0.0))
        conv = tf.nn.conv2d(
                x, kernel, strides=[1, 1, 1, 1], padding='SAME')
        activation = tf.nn.relu(tf.nn.bias_add(conv, bias))
        print_layer(activation)
        return activation

# 定义max pooling层
def maxpool(x, name):
#    activation = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID', name=name)
#    print_layer(activation)
#    return activation
    with tf.name_scope(name):
        activation = tf.nn.max_pool(
                x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
        print(activation)
        return activation

# 定义全连接层
def fc(x, n_out, name, fineturn=False, xavier=False):
    n_in = x.get_shape()[-1].value
#    with tf.name_scope(name) as scope:
#        # 暂不使用预训练模型的参数
##        if fineturn:
##            '''
##            weight = tf.Variable(tf.constant(data_dict[name][0]), name="weights")
##            bias = tf.Variable(tf.constant(data_dict[name][1]), name="bias")
##            '''
##            weight = tf.constant(data_dict[name][0], name="weights")
##            bias = tf.constant(data_dict[name][1], name="bias")
##            print("fineturn")
#        if not xavier:
#            weight = tf.Variable(tf.truncated_normal([n_in, n_out], stddev=0.01), name='weights')
#            bias = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[n_out]),
#                                                trainable=True,
#                                                name='bias')
#            print("truncated_normal")
#        else:
#            weight = tf.get_variable(scope+'weights', shape=[n_in, n_out],
#                                                dtype=tf.float32,
#                                                initializer=tf.contrib.layers.xavier_initializer_conv2d())
#            bias = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[n_out]),
#                                                trainable=True,
#                                                name='bias')
#            print("xavier")
#        # 全连接层可以使用relu_layer函数比较方便，不用像卷积层使用relu函数
#        activation = tf.nn.relu_layer(x, weight, bias, name=name)
#        print_layer(activation)
#        return activation
    with tf.variable_scope(name):
        weigth = tf.get_variable(
                "weight", [n_in, n_out],
                initializer=tf.truncated_normal_initializer(stddev=0.1))
#        regularizer = tf.contrib.layers.l2_regularizer(0.0001)
        bias = tf.get_variable(
                "bias", [n_out], initializer=tf.constant_initializer(0.1))
        activation = tf.nn.relu_layer(x, weigth, bias)
        print(activation)
        return activation


"""
搭建VGG16网络
参数列表：
    images: 输入模型的原始图像数据
    _dropout: 前传输出数据的无效比率
    n_cls: 最终全连接层的输出维度
"""
def VGG16(images, _dropout, n_cls):

    # 暂时不使用预训练模型的卷积层参数
    conv1_1 = conv(images, 64, 'conv1_1')
    conv1_2 = conv(conv1_1, 64, 'conv1_2')
    pool1   = maxpool(conv1_2, 'pool1')

    conv2_1 = conv(pool1, 128, 'conv2_1')
    conv2_2 = conv(conv2_1, 128, 'conv2_2')
    pool2   = maxpool(conv2_2, 'pool2')

    conv3_1 = conv(pool2, 256, 'conv3_1')
    conv3_2 = conv(conv3_1, 256, 'conv3_2')
    conv3_3 = conv(conv3_2, 256, 'conv3_3')
    pool3   = maxpool(conv3_3, 'pool3')

    conv4_1 = conv(pool3, 512, 'conv4_1')
    conv4_2 = conv(conv4_1, 512, 'conv4_2')
    conv4_3 = conv(conv4_2, 512, 'conv4_3')
    pool4   = maxpool(conv4_3, 'pool4')

    conv5_1 = conv(pool4, 512, 'conv5_1')
    conv5_2 = conv(conv5_1, 512, 'conv5_2')
    conv5_3 = conv(conv5_2, 512, 'conv5_3')
    pool5   = maxpool(conv5_3, 'pool5')

    '''
    因为训练自己的数据，全连接层最好不要使用预训练参数
    '''
    flatten  = tf.reshape(pool5, [-1, 7*7*512])
    fc6      = fc(flatten, 4096, 'fc6')
    dropout1 = tf.nn.dropout(fc6, _dropout)

    fc7      = fc(dropout1, 4096, 'fc7')
    dropout2 = tf.nn.dropout(fc7, _dropout)

    fc8      = fc(dropout2, 1000, 'fc8', xavier=True)
    dropout3 = tf.nn.dropout(fc8, _dropout)

    fc9      = fc(dropout3, n_cls, 'fc9', xavier=True)
    # dropout4 = tf.nn.dropout(fc9, _dropout)

    # fc10     = fc(dropout4, 250, 'fc10', xavier=True)
    # dropout5 = tf.nn.dropout(fc10, _dropout)

    # fc11     = fc(dropout5, 1, 'fc11', xavier=True)

    return fc9
