# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 09:31:20 2019

@author: lishjie
@date: 2019/10/16
@descriptoion: 用于设置不同任务的参数配置
"""
# 设置测试图片清晰度任务的参数配置
class definition(object):
    batch_size = 16
    lr = 0.0001
    n_cls = 2         # 设置为2分类问题
    max_steps = 1000
    epoch = 5
