# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 15:45:52 2019

@author: user
"""
import tensorflow as tf

# 用于简化TFRecord文件的格式设置
def _int64_feature(value):
    return tf.train.Feature(int64_list = tf.train.Int64List(value=[value]))

num_shared = 2            # 一共写入的文件个数
instances_pre_shared = 2  # 每个文件中的数据量
for i in range(num_shared):
    filename = ("./data/data.tfrecords-%.5d-of-%.5d" % (i, num_shared))
    writer = tf.python_io.TFRecordWriter(filename)
    # 将数据封装成Example结构并写入TFRecord文件
    for j in range(instances_pre_shared):
        example = tf.train.Example(features=tf.train.Features(feature={
                'i': _int64_feature(i),
                'j': _int64_feature(j)}))
        writer.write(example.SerializeToString())
    writer.close()
